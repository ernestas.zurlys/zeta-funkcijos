# disclaimer: 
# failas 'pjuvis.py' buvo
# pakeistas i klases pavidala
# coding: utf-8

import matplotlib.pyplot as plot

class Zeta:
   def __init__(self, n, m, scale):
## pjūvio detalumo parametras
      self.__n = n 
## detalumas	  
      self.__m = m 
      self.__scale = scale

## koeficientai
   def __factor(self, m): 
      q = [[1 for j in range(i + 1)] for i in range(m)]
      for i in range(1, m):
         q[i][0] = 2 * q[i - 1][0] + 1
         for j in range(1, i):
            q[i][j] = q[i - 1][j - 1] + q[i - 1][j]
      for i in range(m):
         for j in range(i + 1):
            q[i][j] *= pow(-1, j) / pow(2, i + 1)
      return q
	  
## dzeta funkcija
   def __zeta(self, s, q, m): 
      S = 0
      for i in range(m):
         S += q[m - 1][i] / pow(i + 1, s)
      return(S / (1 - pow(2, 1 - s)))

   def calculate(self, t):
      min_x, max_x, min_y, max_y = self.__scale
      m = self.__m
      n = self.__n
## realiosios kreivės taškų aibė
      A = [] 
## menamosios kreivės taškų aibė	  
      B = [] 
      q = self.__factor(m)
      for i in range (n + 1):
         x = min_x + (max_x - min_x) / n * i
         z = self.__zeta(complex(x, t), q, m)
         A += [[x, z.real]]
         if min_y <= z.imag <= max_y:
            B += [[x, z.imag]]
      return A, B

#example
z = Zeta(50, 200, [0, 1, -0.5, 0.5])
real, imaginary = z.calculate(14.1347251417347)

print(str(real) + "\n")
print(str(imaginary) + "\n")
   
#tried to do smth here..
z = Zeta(50, 200, [0, 1, -0.5, 0.5])

zero = [14.1347251417347,21.0220396387716,25.0108575801457,30.4248761258595,32.9350615877392,
37.5861781588257, 40.9187190121475, 43.327073280915, 48.0051508811676, 49.7738324776723]
for j in range(len(zero)):
   real, imaginary = z.calculate(zero[j])
   lr = len(real)
   realX = [None for i in range(lr)]
   realY = [None for i in range(lr)]
   for i in range(lr):
      realX[i] = real[i][0]
      realY[i] = real[i][1]

   li = len(imaginary)
   imaginaryX = [None for i in range(li)]
   imaginaryY = [None for i in range(li)]
   for i in range(li):
      imaginaryX[i] = imaginary[i][0]
      imaginaryY[i] = imaginary[i][1]
   
   plot.plot(realX, realY)
   plot.plot(imaginaryX, imaginaryY)
   plot.show()