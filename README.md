# Example
![Zeta function gif](https://media.giphy.com/media/ZdHwtoInJ7ZuDgvrWl/giphy.gif)


# How to Install
- Requirements:
	- Python 3.7 or later, link: https://www.python.org/downloads/ 

- Installation:
	- Install Python pip, using cmd:
	```
	python -m pip install -U pip
	```
	- Install needed libraries using Python pip:
	```
	pip install numpy
	pip install matplotlib
	pip install imageio
	```

# How to Use
- To start GIF creation of Zeta function all you need is to start generate.py file.
- To change what is generated you can change these constants:
	- "startAt" changes from where on x axis algorithm starts.
	- "iterations" and "increment" are depended from each other (iterations * increment) you can choose how much you want to calculate. 
