# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 10:31:05 2019

@author: Anizcus
"""
import matplotlib
matplotlib.use('Agg')
import matplotlib.patches as mpatches
import matplotlib.pyplot as plot
import imageio as img
from zeta import Zeta
import os
## x tislumas (1/n žingsniai)
n = 50  
## y tikslumas (1/m žingsniai)                
m = 300					
## vidurio atvaizdavimas
middle = n // 2			
## grafų matmenys
axes = [0, 1, -5, 5]	

## sarasas paveiksliuku kurie pus padaryti kaip gif
image_list = []         

## duomenys tam tikru laiku (X)
time_steps = []         
## duomenys tam tikru laiku nuo realios reksmes (Y)
data_realY = []         
## duomenys tam tikru laiku nuo menamos reiksmes (Y)
data_imgsY = []      
   
## iterations ir increment vienas nuo kito priklausomi
iterations = 100	
## todėl pirmo grafiko x ilgis bus toks iterations * increment	
increment = 0.1			
## pradinis taškas nuo kurio kuriami grafikai
startAt = 10			
step = startAt
gif = 'zeta.gif'
path = 'generate-img.png'

diag_title1 = 'Cross-Section of ζ(s) at σ=0.5'
diag_title2 = 'Zeta(σ + '

x_label = 'Time, t'
y_label = 'Re ζ(s), Im ζ(s), s=σ+it'

z = Zeta(n, m, axes)

real_patch = mpatches.Patch(color='blue', label='Re ζ(s)')
imgs_patch = mpatches.Patch(color='orange', label='Im ζ(s)')

print ("Generation: Start")
for x in range(iterations):
   step = step + increment
   
   fig = plot.figure()
   
   ax1 = plot.subplot2grid((1,3), (0,2))
   ax1.set_title(diag_title2 + str(round(step, 3)) + 'i)')
   ax1.set_xlim(axes[0], axes[1])
   ax1.set_ylim(axes[2], axes[3])
      
   real, imaginary = z.calculate(step)
   
   realX = [i[0] for i in real]
   realY = [i[1] for i in real]
   
   imaginaryX = [i[0] for i in imaginary]
   imaginaryY = [i[1] for i in imaginary]
      
   ax1.plot(realX, realY)
   ax1.plot(imaginaryX, imaginaryY)
      
   ax1.axhline(color="red")
   ax1.axvline(color="red")
   
   ax2 = plot.subplot2grid((1,3), (0,0), colspan=2)
   ax2.legend(handles=[real_patch, imgs_patch], loc="lower left")
   ax2.set_title(diag_title1)
   ax2.set_xlabel(x_label)
   ax2.set_ylabel(y_label)
   
   ax2.set_xlim(startAt, startAt + increment * iterations)
   ax2.set_ylim(axes[2], axes[3])
   
   time_steps.append(step)
   data_realY.append(realY[middle])
   data_imgsY.append(imaginaryY[middle])
      
   ax2.plot(time_steps, data_realY)
   ax2.plot(time_steps, data_imgsY)
   
   ax2.axhline(color="red")
   ax2.axvline(color="red")
      
   fig.savefig(path, bbox_inches='tight')
   plot.close(fig)
      
   image_list.append(img.imread(path))
   os.remove(path)
   print("Completed: " + str(x + 1) + "/ " + str(iterations))
img.mimsave(gif, image_list)
print ("Generation: End")
